#!/bin/bash
#OAR -l /nodes=1/core=2,walltime=0:30:0
##OAR -p network_address='dahu110' or network_address='dahu111'
#OAR --project pr-f-image
#OAR -n fcnt2mseed
#OAR -t besteffort
#OAR -t devel

#  =======================================================================================
#   ***  Shell wrapper to submit fcnt2mseed.py in the GriCAD CiGri grid ***
#    
#  =======================================================================================
#   History :  1.0  : 11/2022  : A. Lecointre : Initial version
#               
#  ---------------------------------------------------------------------------------------

# oarsub -S "./submit_grid.sh /bettik/lecoinal/pr-improve/1/1.0.0.fcnt DP2"

set -e

source /applis/site/guix-start.sh
# Suppose that GUIX profile "preprocess" was created with :
# guix install -p $GUIX_USER_PROFILE_DIR/prep_obspy python@3 python-obspy
# Or with a GUIX manifest file
# guix package -m manifest_obspy.scm -p $GUIX_USER_PROFILE_DIR/prep_obspy

refresh_guix prep_obspy

############################################################################################

# args
#if [ $# -ne 2 ]; then
#    echo "ERROR: Bad number of parameters"
#    echo "USAGE: $0 <input mseed file> <channel>"
#    exit 1
#fi

INPUTFILE=$2
CHAN=$3

EXE=fcnt2mseed.py

cat $OAR_NODE_FILE

TMPDIR="/bettik/$USER/RUNDIR/CID_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID"
OUTDIR="/bettik/$USER/CID_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID"

echo $TMPDIR

mkdir -p $TMPDIR
mkdir -p $OUTDIR

cp $EXE $TMPDIR/.

cd $TMPDIR

#ln -s /bettik/lecoinal/pr-improve/1/$INPUTFILE .
ln -s $INPUTFILE .

echo "Convert                       : Date and Time: $(date +%F" "%T"."%N)"

/usr/bin/time -v python3 -u $EXE $INPUTFILE $CHAN

mv $TMPDIR/MSEED $OUTDIR/.

sync

echo "End job                       : Date and Time: $(date +%F" "%T"."%N)"
