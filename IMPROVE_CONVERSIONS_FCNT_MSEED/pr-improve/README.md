Conversion fcnt vers mseed (ou mseed.gz)

[1. Projet IMPROVE Krafla Islande (S. Garambois)](#1-projet-dataset-improve-krafla-islande)

[2. Projet RESOLVE Harmalière](#2-projet-dataset-resolve-harmalière)

---

# 1. Projet Dataset IMPROVE Krafla Islande

depuis cargo, copie des données de summer vers bettik : dans /bettik/lecoinal/pr-improve/1/

```
lecoinal@mantis-cargo:/bettik/lecoinal/pr-improve$ rsync -av ist-oar.isterre:/data/projects/improve/1 . 
```

Ces données d'input ont été déplacées vers Mantis:
```
/bettik/lecoinal/pr-improve
-->
/mantis/home/lecoinal/pr-improve
```
NB : Si elles sont en doublons summer (/data/projets/improve) et Mantis, on pourrait les supprimer de Mantis...


En input :

```
lecoinal@ist-oar:/data/projects/improve/1$ ls
100.0.0.fcnt  18.0.0.fcnt  27.0.0.fcnt  37.0.0.fcnt  46.0.0.fcnt  55.0.0.fcnt  64.0.0.fcnt  73.0.0.fcnt  82.0.0.fcnt  91.0.0.fcnt
10.0.0.fcnt   19.0.0.fcnt  28.0.0.fcnt  38.0.0.fcnt  47.0.0.fcnt  56.0.0.fcnt  65.0.0.fcnt  74.0.0.fcnt  83.0.0.fcnt  92.0.0.fcnt
1.0.0.fcnt    20.0.0.fcnt  29.0.0.fcnt  39.0.0.fcnt  48.0.0.fcnt  57.0.0.fcnt  66.0.0.fcnt  75.0.0.fcnt  84.0.0.fcnt  93.0.0.fcnt
11.0.0.fcnt   2.0.0.fcnt   30.0.0.fcnt  40.0.0.fcnt  49.0.0.fcnt  58.0.0.fcnt  67.0.0.fcnt  76.0.0.fcnt  85.0.0.fcnt  94.0.0.fcnt
12.0.0.fcnt   21.0.0.fcnt  3.0.0.fcnt   4.0.0.fcnt   50.0.0.fcnt  59.0.0.fcnt  68.0.0.fcnt  77.0.0.fcnt  86.0.0.fcnt  95.0.0.fcnt
13.0.0.fcnt   22.0.0.fcnt  31.0.0.fcnt  41.0.0.fcnt  5.0.0.fcnt   60.0.0.fcnt  69.0.0.fcnt  78.0.0.fcnt  87.0.0.fcnt  96.0.0.fcnt
14.0.0.fcnt   23.0.0.fcnt  32.0.0.fcnt  42.1.0.fcnt  51.0.0.fcnt  6.0.0.fcnt   70.0.0.fcnt  79.0.0.fcnt  88.0.0.fcnt  97.0.0.fcnt
15.0.0.fcnt   24.0.0.fcnt  33.0.0.fcnt  43.0.0.fcnt  52.0.0.fcnt  61.0.0.fcnt  7.0.0.fcnt   80.0.0.fcnt  89.0.0.fcnt  99.0.0.fcnt
16.0.0.fcnt   25.0.0.fcnt  35.0.0.fcnt  44.0.0.fcnt  53.0.0.fcnt  62.0.0.fcnt  71.0.0.fcnt  8.0.0.fcnt   90.0.0.fcnt
17.0.0.fcnt   26.0.0.fcnt  36.0.0.fcnt  45.0.0.fcnt  54.0.0.fcnt  63.0.0.fcnt  72.0.0.fcnt  81.0.0.fcnt  9.0.0.fcnt
```

NB : manque 34 et 98, donc seulement 98 fichiers

fichiers de 7GB environ , total 635 GB

---

Pas de creation de projet perseus dédié pr-improve, utilisation des heures d'un autre projet, et donc pas possible d'utiliser les espacées nommés /bettik/PROJECTS/pr-improve

---

On utilise [ce fichier manifest](./manifest_obspy.scm) pour créer un profil GUIX avec python3 et python-obspy

```
# Creation du profil une seule fois pour toutes , à l'aide de [ce fichier manifest](./manifest_obspy.scm):

> source /applis/site/guix-start.sh
> guix package -m manifest_obspy.scm -p $GUIX_USER_PROFILE_DIR/prep_obspy


# puis activation du profil a chaque job

> source /applis/site/guix-start.sh 
Default profile on
The following packages are currently installed in /home/lecoinal/.guix-profile/:
glibc-locales	2.33	out	/gnu/store/nrr24nvf6548if5wdpvxhlvjif3x9jjp-glibc-locales-2.33
> refresh_guix prep_obspy
Activate profile  /var/guix/profiles/per-user/lecoinal/prep_obspy
The following packages are currently installed in /var/guix/profiles/per-user/lecoinal/prep_obspy:
python      	3.9.9	out	/gnu/store/sz7lkmic6qrhfblrhaqaw0fgc4s9n5s3-python-3.9.9
python-obspy	1.2.2	out	/gnu/store/i5kgz4r9d7p303zp06jm10gl169q8ln2-python-obspy-1.2.2
python-h5py 	3.6.0	out	/gnu/store/68nggkk7rxkmxn8k6iadh3fiqyb9cqvs-python-h5py-3.6.0
```

NB : python-h5py n'est pas nécessaire ici

---

On utilise ce code séquentiel [fcnt2mseed.py](./fcnt2mseed.py) pour réaliser la conversion fcnt vers mseed.

Le code est appelé par ce script de soumission [submit_grid.sh](./submit_grid.sh), une exécution par node et par channel.

Pour ces données KRAFLA IMPROVE (Islande), on utilise la revision git 99b562d7

---

Utilisation de cigri : 98*3=294 jobs

98 stations, 3 channels DP2 DP3 DP4

Sur killeen : 
```
> cat improve.jdl 
{
  "name": "improve",
  "resources": "/nodes=1/cpu=1/core=2",
  "exec_file": "{HOME}/pr-improve/submit_grid.sh",
  "exec_directory": "{HOME}/pr-improve",
  "param_file": "{HOME}/IMPROVE/improve_params_chan.txt",
  "test_mode": "false",
  "type": "best-effort",
  "clusters": {
    "dahu": {
      "project": "pr-f-image",
      "walltime": "00:30:00"
    }
  }
}

> cat improve_params_chan.txt 
1 100.0.0.fcnt DP2
2 100.0.0.fcnt DP3
3 100.0.0.fcnt DP4
4 10.0.0.fcnt DP2
5 10.0.0.fcnt DP3
6 10.0.0.fcnt DP4
7 1.0.0.fcnt DP2
8 1.0.0.fcnt DP3
9 1.0.0.fcnt DP4
10 11.0.0.fcnt DP2
[...]
285 95.0.0.fcnt DP4
286 96.0.0.fcnt DP2
287 96.0.0.fcnt DP3
288 96.0.0.fcnt DP4
289 97.0.0.fcnt DP2
290 97.0.0.fcnt DP3
291 97.0.0.fcnt DP4
292 99.0.0.fcnt DP2
293 99.0.0.fcnt DP3
294 99.0.0.fcnt DP4
```

---

Probleme :

Les jobs prennent 8GB, malgré l'option headonly de la méthode obspy.core.read...
J'ai besoin de l'appeler une fois pour connaître le premier et dernier jour de données valide...

Modifier fcnt2mseed.py pour lire jour par jour ? et considérer que premier et dernier jour sont des arguments (connus en amont) ?

Pour l'instant on tourne en séquentiel sur 2 coeurs d'un même noeud de dahu (ainsi on a min 12GB), en besteffort... ca ne gêne pas les autres utilisateurs (ca gêne éventuellement uniquement les autres utilisateurs de dahu en besteffort, mais il n'y avait pas d'autres campagnes cigri en même temps que la mienne).

J'ai laissé tombé luke à cause de la trop forte hétérogénéité de la dotation mémoire des noeuds.



---

Cigri Campaign Id 22192 : total elapsed hours = 0.5 (dahu, 2core/job), job average duration = 10min

Total CPU hours consommés = 10min * 294 jobs * 2 cores = 100 h.


---

Pour regrouper tout dans le même dossier

```
f-dahu:/bettik/login/CID_22192$ for d in $(ls -d oar.*/MSEED) ; do echo $d ; rsync -av $d . ; done
oar.22355898/MSEED
sending incremental file list
MSEED/
MSEED/2022/
MSEED/2022/1/
MSEED/2022/1/100/DP4.D/
MSEED/2022/1/100/DP4.D/1.100.1.DP4.D.2022.176
MSEED/2022/1/100/DP4.D/1.100.1.DP4.D.2022.177
MSEED/2022/1/100/DP4.D/1.100.1.DP4.D.2022.178
[...]
```

---

On retransfert tout sur l'espace /summer du projet :

```
lecoinal@mantis-cargo:~$ rsync -av /bettik/lecoinal/CID_22192/MSEED ist-oar.isterre:/data/projects/improve/lecoinal/1/.
```

---

Info : la compression gzip semble intéressante sur ces données MSEED Float32

```
lecoinal@f-dahu:/bettik/lecoinal/CID_22192/oar.22356186$ gzip --recursive -v MSEED/
MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.186:	 50.1% -- replaced with MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.186.gz
MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.191:	 49.6% -- replaced with MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.191.gz
MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.196:	 54.5% -- replaced with MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.196.gz
MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.176:	 37.7% -- replaced with MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.176.gz
MSEED//2022/1/94/DP3.D/1.94.1.DP3.D.2022.187:	^C
lecoinal@f-dahu:/bettik/lecoinal/CID_22192/oar.22356186$ cd ../oar.22356114
lecoinal@f-dahu:/bettik/lecoinal/CID_22192/oar.22356114$ gzip --recursive -v MSEED/
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.185:	 45.8% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.185.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.195:	 54.9% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.195.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.174:	 52.8% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.174.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.198:	 53.9% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.198.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.172:	 46.1% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.172.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.187:	 54.3% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.187.gz
MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.170:	 41.7% -- replaced with MSEED//2022/1/73/DP3.D/1.73.1.DP3.D.2022.170.gz
[...]
```

---

# 2. Projet Dataset RESOLVE Harmalière

git revision : eaf45f7f

99 nodes : node 1 ... node 100 (excepted node 32)

```
> ls -lh fcnt/harm*/Other/1/*
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harm51-52-53/Other/1/51.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harm51-52-53/Other/1/52.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harm51-52-53/Other/1/53.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/1.1.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/10.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/100.2.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/11.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/12.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/13.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/14.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/15.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/16.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/17.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/18.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/19.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/2.1.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/20.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/21.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/22.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/23.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/24.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/25.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/26.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/27.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/28.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/29.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.0G Jul  9  2021 fcnt/harmaliere/Other/1/3.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/30.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/31.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/33.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/34.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/35.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/36.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.2G Jul  9  2021 fcnt/harmaliere/Other/1/37.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/38.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.0G Jul  9  2021 fcnt/harmaliere/Other/1/39.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/4.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.3G Jul  9  2021 fcnt/harmaliere/Other/1/40.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/41.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/42.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/43.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/44.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/45.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/46.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/47.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/48.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/49.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.1G Jul  9  2021 fcnt/harmaliere/Other/1/5.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/50.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/54.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/55.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/56.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/57.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/58.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/59.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.2G Jul  9  2021 fcnt/harmaliere/Other/1/6.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.0G Jul  9  2021 fcnt/harmaliere/Other/1/60.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/61.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.3G Jul  9  2021 fcnt/harmaliere/Other/1/62.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/63.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/64.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 7.8G Jul  9  2021 fcnt/harmaliere/Other/1/65.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/66.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/67.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/68.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/69.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/7.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/70.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/71.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.9G Jul  9  2021 fcnt/harmaliere/Other/1/72.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/73.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/74.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/75.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/76.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/77.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/78.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/79.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/8.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/80.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/81.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/82.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/83.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/84.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/85.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/86.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/87.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/88.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/89.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/9.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/90.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.6G Jul  9  2021 fcnt/harmaliere/Other/1/91.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/92.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/93.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.4G Jul  9  2021 fcnt/harmaliere/Other/1/94.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.7G Jul  9  2021 fcnt/harmaliere/Other/1/95.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.2G Jul  9  2021 fcnt/harmaliere/Other/1/96.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 9.0G Jul  9  2021 fcnt/harmaliere/Other/1/97.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.8G Jul  9  2021 fcnt/harmaliere/Other/1/98.0.0.fcnt
-rwxrwxrwx 1 lecoinal l-isterre 8.5G Jul  9  2021 fcnt/harmaliere/Other/1/99.0.0.fcnt
```

```
>  du -sch fcnt/harm*          
27G	fcnt/harm51-52-53
831G	fcnt/harmaliere
858G	total
```

Firstly we scan the entire dataset with obspy.core.read headonly option (testmode=True in fcnt2mseed.py)

We get these lists of dates and channels available in FCNT files

```
# ou (date de debut ou de fin) grep samples 22355/*out| awk '{print $5}' | awk -FT '{print $1}'|sort -u
> grep samples 22355/*out| awk '{print $5}' | awk -FT '{print $1}'|sort -u
2021-05-25
2021-05-26
2021-05-27
2021-05-28
2021-05-29
2021-05-30
2021-05-31
2021-06-01
2021-06-02
2021-06-03
2021-06-04
2021-06-05
2021-06-06
2021-06-07
2021-06-08
2021-06-09
2021-06-10
2021-06-11
2021-06-12
2021-06-13
2021-06-14
2021-06-15
2021-06-16
2021-06-17
2021-06-18
2021-06-19
2021-06-20
2021-06-21
2021-06-22
2021-06-23
2021-06-24
2021-06-25
2021-06-26
2021-06-27
2021-06-28
2021-06-29
2021-06-30
2021-07-01
2021-07-02
> $ grep samples 22355/*out| awk '{print $1}' | awk -F: '{print $2}'| sort -u| awk -F. '{print $4}'|sort -u
DP2
DP3
DP4
```

Thus we create this CiGri Param file (297 jobs) :
```
>  cat harmaliere_params_chan.txt
1 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/51.0.0.fcnt	DP2	False
2 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/51.0.0.fcnt	DP3	False
3 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/51.0.0.fcnt	DP4	False
4 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/52.0.0.fcnt	DP2	False
5 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/52.0.0.fcnt	DP3	False
6 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/52.0.0.fcnt	DP4	False
7 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/53.0.0.fcnt	DP2	False
8 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/53.0.0.fcnt	DP3	False
9 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harm51-52-53/Other/1/53.0.0.fcnt	DP4	False
10 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/1.1.0.fcnt	DP2	False
11 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/1.1.0.fcnt	DP3	False
12 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/1.1.0.fcnt	DP4	False
13 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/10.0.0.fcnt	DP2	False
14 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/10.0.0.fcnt	DP3	False
15 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/10.0.0.fcnt	DP4	False
16 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/100.2.0.fcnt	DP2	False
17 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/100.2.0.fcnt	DP3	False
18 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/100.2.0.fcnt	DP4	False
19 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/11.0.0.fcnt	DP2	False
20 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/11.0.0.fcnt	DP3	False
21 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/11.0.0.fcnt	DP4	False
22 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/12.0.0.fcnt	DP2	False
23 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/12.0.0.fcnt	DP3	False
24 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/12.0.0.fcnt	DP4	False
25 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/13.0.0.fcnt	DP2	False
26 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/13.0.0.fcnt	DP3	False
27 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/13.0.0.fcnt	DP4	False
28 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/14.0.0.fcnt	DP2	False
29 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/14.0.0.fcnt	DP3	False
30 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/14.0.0.fcnt	DP4	False
31 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/15.0.0.fcnt	DP2	False
32 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/15.0.0.fcnt	DP3	False
33 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/15.0.0.fcnt	DP4	False
34 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/16.0.0.fcnt	DP2	False
35 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/16.0.0.fcnt	DP3	False
36 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/16.0.0.fcnt	DP4	False
37 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/17.0.0.fcnt	DP2	False
38 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/17.0.0.fcnt	DP3	False
39 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/17.0.0.fcnt	DP4	False
40 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/18.0.0.fcnt	DP2	False
41 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/18.0.0.fcnt	DP3	False
42 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/18.0.0.fcnt	DP4	False
43 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/19.0.0.fcnt	DP2	False
44 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/19.0.0.fcnt	DP3	False
45 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/19.0.0.fcnt	DP4	False
46 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/2.1.0.fcnt	DP2	False
47 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/2.1.0.fcnt	DP3	False
48 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/2.1.0.fcnt	DP4	False
49 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/20.0.0.fcnt	DP2	False
50 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/20.0.0.fcnt	DP3	False
51 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/20.0.0.fcnt	DP4	False
52 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/21.0.0.fcnt	DP2	False
53 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/21.0.0.fcnt	DP3	False
54 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/21.0.0.fcnt	DP4	False
55 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/22.0.0.fcnt	DP2	False
56 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/22.0.0.fcnt	DP3	False
57 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/22.0.0.fcnt	DP4	False
58 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/23.0.0.fcnt	DP2	False
59 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/23.0.0.fcnt	DP3	False
60 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/23.0.0.fcnt	DP4	False
61 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/24.0.0.fcnt	DP2	False
62 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/24.0.0.fcnt	DP3	False
63 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/24.0.0.fcnt	DP4	False
64 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/25.0.0.fcnt	DP2	False
65 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/25.0.0.fcnt	DP3	False
66 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/25.0.0.fcnt	DP4	False
67 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/26.0.0.fcnt	DP2	False
68 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/26.0.0.fcnt	DP3	False
69 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/26.0.0.fcnt	DP4	False
70 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/27.0.0.fcnt	DP2	False
71 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/27.0.0.fcnt	DP3	False
72 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/27.0.0.fcnt	DP4	False
73 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/28.0.0.fcnt	DP2	False
74 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/28.0.0.fcnt	DP3	False
75 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/28.0.0.fcnt	DP4	False
76 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/29.0.0.fcnt	DP2	False
77 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/29.0.0.fcnt	DP3	False
78 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/29.0.0.fcnt	DP4	False
79 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/3.0.0.fcnt	DP2	False
80 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/3.0.0.fcnt	DP3	False
81 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/3.0.0.fcnt	DP4	False
82 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/30.0.0.fcnt	DP2	False
83 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/30.0.0.fcnt	DP3	False
84 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/30.0.0.fcnt	DP4	False
85 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/31.0.0.fcnt	DP2	False
86 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/31.0.0.fcnt	DP3	False
87 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/31.0.0.fcnt	DP4	False
88 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/33.0.0.fcnt	DP2	False
89 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/33.0.0.fcnt	DP3	False
90 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/33.0.0.fcnt	DP4	False
91 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/34.0.0.fcnt	DP2	False
92 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/34.0.0.fcnt	DP3	False
93 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/34.0.0.fcnt	DP4	False
94 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/35.0.0.fcnt	DP2	False
95 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/35.0.0.fcnt	DP3	False
96 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/35.0.0.fcnt	DP4	False
97 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/36.0.0.fcnt	DP2	False
98 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/36.0.0.fcnt	DP3	False
99 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/36.0.0.fcnt	DP4	False
100 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/37.0.0.fcnt	DP2	False
101 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/37.0.0.fcnt	DP3	False
102 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/37.0.0.fcnt	DP4	False
103 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/38.0.0.fcnt	DP2	False
104 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/38.0.0.fcnt	DP3	False
105 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/38.0.0.fcnt	DP4	False
106 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/39.0.0.fcnt	DP2	False
107 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/39.0.0.fcnt	DP3	False
108 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/39.0.0.fcnt	DP4	False
109 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/4.0.0.fcnt	DP2	False
110 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/4.0.0.fcnt	DP3	False
111 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/4.0.0.fcnt	DP4	False
112 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/40.0.0.fcnt	DP2	False
113 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/40.0.0.fcnt	DP3	False
114 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/40.0.0.fcnt	DP4	False
115 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/41.0.0.fcnt	DP2	False
116 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/41.0.0.fcnt	DP3	False
117 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/41.0.0.fcnt	DP4	False
118 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/42.0.0.fcnt	DP2	False
119 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/42.0.0.fcnt	DP3	False
120 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/42.0.0.fcnt	DP4	False
121 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/43.0.0.fcnt	DP2	False
122 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/43.0.0.fcnt	DP3	False
123 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/43.0.0.fcnt	DP4	False
124 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/44.0.0.fcnt	DP2	False
125 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/44.0.0.fcnt	DP3	False
126 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/44.0.0.fcnt	DP4	False
127 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/45.0.0.fcnt	DP2	False
128 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/45.0.0.fcnt	DP3	False
129 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/45.0.0.fcnt	DP4	False
130 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/46.0.0.fcnt	DP2	False
131 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/46.0.0.fcnt	DP3	False
132 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/46.0.0.fcnt	DP4	False
133 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/47.0.0.fcnt	DP2	False
134 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/47.0.0.fcnt	DP3	False
135 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/47.0.0.fcnt	DP4	False
136 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/48.0.0.fcnt	DP2	False
137 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/48.0.0.fcnt	DP3	False
138 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/48.0.0.fcnt	DP4	False
139 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/49.0.0.fcnt	DP2	False
140 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/49.0.0.fcnt	DP3	False
141 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/49.0.0.fcnt	DP4	False
142 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/5.0.0.fcnt	DP2	False
143 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/5.0.0.fcnt	DP3	False
144 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/5.0.0.fcnt	DP4	False
145 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/50.0.0.fcnt	DP2	False
146 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/50.0.0.fcnt	DP3	False
147 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/50.0.0.fcnt	DP4	False
148 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/54.0.0.fcnt	DP2	False
149 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/54.0.0.fcnt	DP3	False
150 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/54.0.0.fcnt	DP4	False
151 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/55.0.0.fcnt	DP2	False
152 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/55.0.0.fcnt	DP3	False
153 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/55.0.0.fcnt	DP4	False
154 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/56.0.0.fcnt	DP2	False
155 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/56.0.0.fcnt	DP3	False
156 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/56.0.0.fcnt	DP4	False
157 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/57.0.0.fcnt	DP2	False
158 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/57.0.0.fcnt	DP3	False
159 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/57.0.0.fcnt	DP4	False
160 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/58.0.0.fcnt	DP2	False
161 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/58.0.0.fcnt	DP3	False
162 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/58.0.0.fcnt	DP4	False
163 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/59.0.0.fcnt	DP2	False
164 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/59.0.0.fcnt	DP3	False
165 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/59.0.0.fcnt	DP4	False
166 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/6.0.0.fcnt	DP2	False
167 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/6.0.0.fcnt	DP3	False
168 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/6.0.0.fcnt	DP4	False
169 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/60.0.0.fcnt	DP2	False
170 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/60.0.0.fcnt	DP3	False
171 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/60.0.0.fcnt	DP4	False
172 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/61.0.0.fcnt	DP2	False
173 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/61.0.0.fcnt	DP3	False
174 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/61.0.0.fcnt	DP4	False
175 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/62.0.0.fcnt	DP2	False
176 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/62.0.0.fcnt	DP3	False
177 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/62.0.0.fcnt	DP4	False
178 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/63.0.0.fcnt	DP2	False
179 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/63.0.0.fcnt	DP3	False
180 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/63.0.0.fcnt	DP4	False
181 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/64.0.0.fcnt	DP2	False
182 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/64.0.0.fcnt	DP3	False
183 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/64.0.0.fcnt	DP4	False
184 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/65.0.0.fcnt	DP2	False
185 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/65.0.0.fcnt	DP3	False
186 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/65.0.0.fcnt	DP4	False
187 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/66.0.0.fcnt	DP2	False
188 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/66.0.0.fcnt	DP3	False
189 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/66.0.0.fcnt	DP4	False
190 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/67.0.0.fcnt	DP2	False
191 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/67.0.0.fcnt	DP3	False
192 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/67.0.0.fcnt	DP4	False
193 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/68.0.0.fcnt	DP2	False
194 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/68.0.0.fcnt	DP3	False
195 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/68.0.0.fcnt	DP4	False
196 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/69.0.0.fcnt	DP2	False
197 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/69.0.0.fcnt	DP3	False
198 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/69.0.0.fcnt	DP4	False
199 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/7.0.0.fcnt	DP2	False
200 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/7.0.0.fcnt	DP3	False
201 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/7.0.0.fcnt	DP4	False
202 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/70.0.0.fcnt	DP2	False
203 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/70.0.0.fcnt	DP3	False
204 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/70.0.0.fcnt	DP4	False
205 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/71.0.0.fcnt	DP2	False
206 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/71.0.0.fcnt	DP3	False
207 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/71.0.0.fcnt	DP4	False
208 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/72.0.0.fcnt	DP2	False
209 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/72.0.0.fcnt	DP3	False
210 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/72.0.0.fcnt	DP4	False
211 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/73.0.0.fcnt	DP2	False
212 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/73.0.0.fcnt	DP3	False
213 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/73.0.0.fcnt	DP4	False
214 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/74.0.0.fcnt	DP2	False
215 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/74.0.0.fcnt	DP3	False
216 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/74.0.0.fcnt	DP4	False
217 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/75.0.0.fcnt	DP2	False
218 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/75.0.0.fcnt	DP3	False
219 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/75.0.0.fcnt	DP4	False
220 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/76.0.0.fcnt	DP2	False
221 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/76.0.0.fcnt	DP3	False
222 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/76.0.0.fcnt	DP4	False
223 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/77.0.0.fcnt	DP2	False
224 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/77.0.0.fcnt	DP3	False
225 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/77.0.0.fcnt	DP4	False
226 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/78.0.0.fcnt	DP2	False
227 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/78.0.0.fcnt	DP3	False
228 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/78.0.0.fcnt	DP4	False
229 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/79.0.0.fcnt	DP2	False
230 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/79.0.0.fcnt	DP3	False
231 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/79.0.0.fcnt	DP4	False
232 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/8.0.0.fcnt	DP2	False
233 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/8.0.0.fcnt	DP3	False
234 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/8.0.0.fcnt	DP4	False
235 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/80.0.0.fcnt	DP2	False
236 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/80.0.0.fcnt	DP3	False
237 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/80.0.0.fcnt	DP4	False
238 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/81.0.0.fcnt	DP2	False
239 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/81.0.0.fcnt	DP3	False
240 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/81.0.0.fcnt	DP4	False
241 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/82.0.0.fcnt	DP2	False
242 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/82.0.0.fcnt	DP3	False
243 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/82.0.0.fcnt	DP4	False
244 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/83.0.0.fcnt	DP2	False
245 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/83.0.0.fcnt	DP3	False
246 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/83.0.0.fcnt	DP4	False
247 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/84.0.0.fcnt	DP2	False
248 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/84.0.0.fcnt	DP3	False
249 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/84.0.0.fcnt	DP4	False
250 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/85.0.0.fcnt	DP2	False
251 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/85.0.0.fcnt	DP3	False
252 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/85.0.0.fcnt	DP4	False
253 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/86.0.0.fcnt	DP2	False
254 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/86.0.0.fcnt	DP3	False
255 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/86.0.0.fcnt	DP4	False
256 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/87.0.0.fcnt	DP2	False
257 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/87.0.0.fcnt	DP3	False
258 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/87.0.0.fcnt	DP4	False
259 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/88.0.0.fcnt	DP2	False
260 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/88.0.0.fcnt	DP3	False
261 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/88.0.0.fcnt	DP4	False
262 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/89.0.0.fcnt	DP2	False
263 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/89.0.0.fcnt	DP3	False
264 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/89.0.0.fcnt	DP4	False
265 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/9.0.0.fcnt	DP2	False
266 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/9.0.0.fcnt	DP3	False
267 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/9.0.0.fcnt	DP4	False
268 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/90.0.0.fcnt	DP2	False
269 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/90.0.0.fcnt	DP3	False
270 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/90.0.0.fcnt	DP4	False
271 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/91.0.0.fcnt	DP2	False
272 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/91.0.0.fcnt	DP3	False
273 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/91.0.0.fcnt	DP4	False
274 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/92.0.0.fcnt	DP2	False
275 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/92.0.0.fcnt	DP3	False
276 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/92.0.0.fcnt	DP4	False
277 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/93.0.0.fcnt	DP2	False
278 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/93.0.0.fcnt	DP3	False
279 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/93.0.0.fcnt	DP4	False
280 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/94.0.0.fcnt	DP2	False
281 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/94.0.0.fcnt	DP3	False
282 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/94.0.0.fcnt	DP4	False
283 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/95.0.0.fcnt	DP2	False
284 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/95.0.0.fcnt	DP3	False
285 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/95.0.0.fcnt	DP4	False
286 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/96.0.0.fcnt	DP2	False
287 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/96.0.0.fcnt	DP3	False
288 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/96.0.0.fcnt	DP4	False
289 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/97.0.0.fcnt	DP2	False
290 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/97.0.0.fcnt	DP3	False
291 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/97.0.0.fcnt	DP4	False
292 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/98.0.0.fcnt	DP2	False
293 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/98.0.0.fcnt	DP3	False
294 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/98.0.0.fcnt	DP4	False
295 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/99.0.0.fcnt	DP2	False
296 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/99.0.0.fcnt	DP3	False
297 /bettik/PROJECTS/pr-resolve/lecoinal/tmp/20230314_HARDDRIVE_HARMALIERE/fcnt/harmaliere/Other/1/99.0.0.fcnt	DP4	False
```

We submit a CiGri campaign on dahu cluster (2cores/job, at least 6GB/core)

```
>  cat harmaliere.jdl 
{
  "name": "harmaliere",
  "resources": "/nodes=1/cpu=1/core=2",
  "exec_file": "{HOME}/tmp/UTILS/IMPROVE_CONVERSIONS_FCNT_MSEED/pr-improve/submit_grid.sh",
  "exec_directory": "{HOME}/tmp/UTILS/IMPROVE_CONVERSIONS_FCNT_MSEED/pr-improve",
  "param_file": "{HOME}/IMPROVE/harmaliere_params_chan.txt",
  "test_mode": "false",
  "type": "best-effort",
  "clusters": {
    "dahu": {
      "project": "pr-resolve",
      "walltime": "00:30:00"
    }
  }
}
```

CiGri campaign id : 22356 : 
 - 10min/job
 - total elapsed time = 2h
 - total CPU time = `297jobs * 2cores * 650s = 100 core-hours`

Then merge all outfiles and transfer from /bettik to /summer :
```
> cd /bettik/login/CID_22356
> for d in $(ls -d oar.*/MSEED) ; do echo $d ; rsync -av $d . ; done 
> rsync -av MSEED /summer/resolve/Harmaliere/DATA_MSEED/.
> du -sch /summer/resolve/Harmaliere/DATA_MSEED/MSEED
874G    /summer/resolve/Harmaliere/DATA_MSEED/MSEED
874G    total
```
