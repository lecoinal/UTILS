#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of IMPROVE project : a python code for converting      #
#    FCNT files to MSEED files for the dataset from ???                       #
#                                                                             #
#    Copyright (C) 2022 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script fcnt2mseed.py  ***
#   This is the main Python script to convert FCNT files to MSEED files 
#    
#  =======================================================================================
#   History :  1.0  : 11/2022  : A. Lecointre : Initial Python version
#	        
#  ---------------------------------------------------------------------------------------
#   methodology   : 
#                 : 
#
#
#  ---------------------------------------------------------------------------------------

from obspy.core import read,UTCDateTime
import os

def fcnt2mseed(df,ch,testmode):
	
  # Read only metadata from header
  s=read(df,headonly=True)

  print(len(s))
  print(s.__str__(extended=True))

  firstyear = s[0].stats.starttime.year
  lastyear = s[-1].stats.endtime.year
  if firstyear != lastyear:
    print("error, firstyear:",firstyear," lastyear:",lastyear," -- abort")
    sys.exit(1)
  else:
    singleyear=firstyear
    print("year:",singleyear)

  if testmode == False:

    firstjd = s[0].stats.starttime.julday
    lastjd = s[-1].stats.endtime.julday
    print("firstjd:",firstjd)
    print("lastjd:",lastjd)
  
    for it in range(len(s)):
      tr = s[it]
      net = tr.stats.network
      sta = tr.stats.station
      loc = tr.stats.location
      channel = tr.stats.channel
      jd = tr.stats.starttime.julday
      datatype = tr.data.dtype
      print("trace: ",it,net,sta,loc,channel,jd,datatype)
  
    # Now read the data (only one channel)
    s = read(df).select(channel=ch)
  
    # Loop on the julday for writing one mseed file per day
    for ijd in range(firstjd,lastjd+1):
      print(ijd)
      dt = UTCDateTime(year=singleyear, julday=ijd, hour=0, minute=0)
      st0 = s.slice(dt, dt + 86400 - s[0].stats.delta)
      net = st0[0].stats.network
      sta = st0[0].stats.station
      loc = st0[0].stats.location
      channel = st0[0].stats.channel
      jd = st0[0].stats.starttime.julday
  
      SDSdir = './MSEED/'+"{:04d}".format(singleyear)+'/'+net+'/'+sta+'/'+channel+'.D/'
      SDSfile = net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(singleyear)+'.'+"{:03d}".format(jd)
      try:
        os.stat(SDSdir)
      except:
        os.makedirs(SDSdir)
      st0.write(SDSdir+'/'+SDSfile, format='MSEED' , reclen=4096 , encoding='FLOAT32' )
  
if __name__ == '__main__':
  import sys,time
  t=time.time()
  fcnt2mseed(sys.argv[1],sys.argv[2],False)
  print(sys.argv[1],sys.argv[2],time.time()-t," sec")
