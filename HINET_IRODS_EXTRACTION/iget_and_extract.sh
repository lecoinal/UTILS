#!/bin/bash

source /applis/site/nix.sh

nix-env --switch-profile $NIX_USER_PROFILE_DIR/extraction

nix-env -iA ciment-channel.gnutar

tar --version

for f in $(cat list_extract.txt) ; do 
  date
  echo $f
  ils -L /cigri/home/lecoin56/RAW_MSEED_DAILY_TAR/$f
  iget -v /cigri/home/lecoin56/RAW_MSEED_DAILY_TAR/$f
  tar --files-from HiNet_sta_selected_filefrom.txt -xvf $f
  rm $f
done

