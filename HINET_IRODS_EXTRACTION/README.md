**Extraction des HiNET TAR d'iRODS**

Exemple pour extraire cette liste de stations sur cette période: 

* du 01/01/2010 au 14/02/2010
* du 20/07/2010 au 21/08/2010

```
N.FKSH 38.7642 140.3766 0
N.FSWH 38.8654 141.3512 0
N.HMNH 39.4559 141.0001 0
N.HMSH 39.3434 141.0473 0
N.HNRH 39.1740 140.7129 0
N.ICEH 38.9690 141.0012 0
N.IWNH 38.1133 140.8441 0
N.KAKH 38.5158 141.3421 0
N.KGSH 39.1979 141.0118 0
N.KKWH 38.9207 141.6377 0
N.KMAH 37.6636 140.5974 0
N.KMIH 39.2741 141.8233 0
N.KMYH 38.0813 140.2978 0
N.KRYH 37.3530 140.4264 0
N.KWSH 38.1802 140.6405 0
N.MGMH 38.7132 140.5546 0
N.MKJH 37.4703 140.7227 0
N.MRUH 37.4894 140.5380 0
N.NGUH 37.1616 140.0930 0
N.NKEH 38.4257 140.1249 0
N.NKWH 38.3860 139.9916 0
N.NNMH 37.2822 140.2144 0
N.NRKH 38.8587 140.6513 0
N.NYOH 38.1035 140.1552 0
N.OGCH 38.9801 140.4952 0
N.OMRH 39.3548 140.3869 0
N.ONDH 38.5793 140.7804 0
N.RZTH 39.0307 141.5320 0
N.SMTH 39.1809 141.3909 0
N.THTH 37.9633 140.1836 0
N.TOUH 39.3340 141.3015 0
N.TOWH 38.7860 141.3254 0
N.YUZH 39.1912 140.4710 0
```

1) On récupère la liste des archives journalières (tar ou tgz) présentes sous iRODS, on la renseigne dans ce fichier : list_extract.txt

2) On indique la liste des stations que l'on souhaite extraire, on la renseigne dans ce fichier :  HiNet_sta_selected_filefrom.txt

3) on exécute le script, de préférence dans un tmux sur la frontale dahu ou luke (nécessite NIX)

```
./iget_and_extract.sh  > monextraction.logout 2> monextraction.logerr
```
