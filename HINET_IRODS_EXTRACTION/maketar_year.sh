#!/bin/bash

# Usage:
# In a tmux, 
# ./maketar.sh 2008 > maketar_2008.logout 2> maketar_2008.logerr


set -e

source /applis/site/nix.sh

nix-env --switch-profile $NIX_USER_PROFILE_DIR/extraction

nix-env -iA ciment-channel.gnutar

tar --version

year="$1"

mkdir -p $year

cd $year

#for year in $(seq 2008 2011) ; do
  for jd in $(seq -w 1 366) ; do
    date
    START=$(date +%s.%N)

    if [ -s MSEED_${year}${jd}_TL.tar ] ; then # exists and size > 0

      echo "MSEED_${year}${jd}_TL.tar already exists, skipping..."

    else

      iquest --no-page "select COLL_NAME,DATA_NAME where COLL_NAME like '/cigri/home/lecoin56/${year}/ZZ%' and DATA_NAME like '%.${year}.${jd}%'" > zzzzz 
      if [ -s zzzzz ] ; then  # exists and size > 0
        rm -rf tmp
        grep COLL_NAME zzzzz | awk '{print $3}' > zirodsdir
        grep DATA_NAME zzzzz | awk '{print $3}' > zfile
        paste -d "/" zirodsdir zfile > zirodsfile
        sed -e "s/\/cigri\/home\/lecoin56\///" zirodsdir > zsdsdir
        paste zirodsfile zsdsdir zfile > zall
        rm zirodsfile zsdsdir zfile
        while read line ; do
          irodsfile=$(echo $line | awk '{print $1}')
          sdsdir=$(echo $line | awk '{print $2}')
          sdsfile=$(echo $line | awk '{print $3}')
          mkdir -p tmp/$sdsdir
          ils -L $irodsfile
          iget -v $irodsfile
          mv $sdsfile tmp/$sdsdir/.
        done < zall
        cd tmp
        tar -cvf MSEED_${year}${jd}_TL.tar *
        mv MSEED_${year}${jd}_TL.tar ..
        cd ..
        rm -rf tmp
      fi
    fi
    END=$(date +%s.%N)
    DIFF=$(echo "$END - $START" | bc)
    echo "$year $jd ElapsedTime: $DIFF sec"
  done
#done
