#!/bin/bash

#set -x

# script de conversion des données japonaise WIN (CNT) vers MSEED
# input: fichier 1 min, tous canaux/stations concaténés, format CNT
# output: fichier MSEED, journaliers, par canal, par station

# 1. Convert from CNT (1min, all channels/stations in the same cntfile) to SAC (1min, 1 sacfile/channel/station)
#    using win2sac_32
# 2. Convert from SAC (1min, 1 sacfile/channel/station) to MSEED (1min, 1 mseedfile/channel/station)
#    using sac2mseed_1.11
# 3. Convert from MSEED (1min, 1 mseedfile/channel/station) to MSEED (1day, 1 mseedfile/channel/station)
#    using cat

# output data structure: SDS: YEAR/NET/STA/CHAN.TYPE/NET.STA.LOC.CHAN.TYPE.YEAR.JDAY
#                             Exemple: 2011/ZZ/WNNH/EHE.D/ZZ.WNNH.00.EHE.D.2011.070

usage() {
	echo "Convert WIN (=CNT=DATAMARK) data (1 CNTfile / min, all channels in the same file)"
	echo "to MSEED data (1 daily MSEEDfile / channel / station)"
	echo "Use win2sac_32 (ref ? ohmi ???)"
	echo "Use sac2mseed_v1.11 (seiscode: https://seiscode.iris.washington.edu/projects/sac2mseed)"
        echo " "
	echo USAGE: $(basename $0) [-h] [-t TAG] [-d DATATYPE]
        echo "     -h : print this message "
        echo "     -t : tag as YYYYMMDD "
        echo "     -d : datatype as VM (HiNet) or TL (TiltMeter) "
	echo " " 
	echo "Warning: mseed encoding float32 ???"
	echo "Warning: gap in WIN input file ? -> zero !" 
        exit 0
}

while getopts :ht:d:  opt
  do
    case $opt in
      (h)  usage ;;
      (t)  TAG=$OPTARG ;;
      (d)  DATATYPE=$OPTARG ;;
      (\?) echo $( basename $0 ): option -$OPTARG not valid. ; usage ;;
    esac
  done

# CUSTOMIZE HERE ################################

WIN2SAC="${HOME}/bin/win2sac_32"
SAC2MSEED="${HOME}/bin/sac2mseed"

ENCODING="FLOAT32"

SLEEP_TRAVAILLEUR=1
SLEEP_ORDONNANCEUR=1

# DO NOT CHANGE AFTER THIS LINE #################

TYPE="D"
NET="ZZ"
LOC="00"

IDIR="./DATA_CNT/"
ODIR="./"
LOCK="./LOCK/"

echo "Processing: $TAG 00 -2     Date and Time: $(date +%F" "%T"."%N)"

CURRENT_CHANNELTBL="channels.tbl.$TAG"

DATATYPE_A=${DATATYPE:0:1}
DATATYPE_B=${DATATYPE:1:1}

YYYY=${TAG:0:4}
MM=${TAG:4:2}
DD=${TAG:6:2}

#WINDIR="${IDIR}/$YYYY/$MM/$DD/01/01/$DATATYPE_A/$DATATYPE_B/"
WINDIR="./$DATATYPE_B/"

case $ENCODING in
  ("FLOAT32") ENC="-e 4" ;;
  ("STEIM2")  ENC=" "    ;;
esac

case $DATATYPE in
  ("VME") PMAX=6000 ;;
  ("VMN") PMAX=6000 ;;
  ("VMU") PMAX=6000 ;;
  ("TL") PMAX=1200 ;;
esac

cat ${WINDIR}/channels.tbl.${TAG}

# si TL : supprimer les lignes contenant e00f N.HA2H et e01f N.HA2H
#sed -e "/^e0[10]f.*N.HA2H *L[EN].*$/d" \
#    -e "/^#.*$/d" \
#    -e "/^.*H *BAR *.*$/d" \
#    -e "/^.*H *[ENUXY] *.*$/d" \
#    -e "/^.*H *[EN][12] *.*$/d" \
#    -e "/^.*H *H[ENUXY] *.*$/d" \
#    -e "/^.*H *[ENU]A1 *.*$/d" \
#    -e "/^.*H *L[EN]2 *.*$/d" \
#    -e "/^.*H *LU *.*$/d" \
#    -e "/^.*H *S0[0B] *.*$/d" \
#    -e "/^.*H *S1[2B] *.*$/d" \
#    -e "/^.*H *S2[4B] *.*$/d" \
#    -e "/^.*H *U[12] *.*$/d" \
#    -e "/^.*H *SZ *.*$/d" ${WINDIR}/channels.tbl.${TAG} > channels.tbl.$TAG

# si TL : supprimer les lignes contenant e00f N.HA2H et e01f N.HA2H
# le champ channel ($5) ne peut etre que E N U X Y LE LN LX LY

case $DATATYPE in
  ("VME") sed -e "/^#.*$/d" ${WINDIR}/channels.tbl.${TAG} | awk '{if ($5 =="E") print $0;}' > channels.tbl.$TAG ;;
  ("VMN") sed -e "/^#.*$/d" ${WINDIR}/channels.tbl.${TAG} | awk '{if ($5 == "N") print $0;}' > channels.tbl.$TAG ;;
  ("VMU") sed -e "/^#.*$/d" ${WINDIR}/channels.tbl.${TAG} | awk '{if ($5 == "U" || $5 == "X" || $5 == "Y") print $0;}' > channels.tbl.$TAG ;;
  ("TL") sed -e "/^#.*$/d" -e "/^e0[10]f.*N.HA2H *L[EN].*$/d" ${WINDIR}/channels.tbl.${TAG} | awk '{if ($5 == "LE" || $5 == "LN" || $5 == "LX" || $5 == "LY") print $0;}' > channels.tbl.$TAG ;;
esac

sed -e "s/<<CHANNELTBL>>/.\/$CURRENT_CHANNELTBL/" win_skel.prm > win.prm.$TAG
NBCHANNEL=$(wc -l $CURRENT_CHANNELTBL | awk '{print $1}')
echo "NBCHANNEL $NBCHANNEL"

tag2jul(){
    # usage tag2jul year month day
    year=$(( 10#$1 ));  month=$(( 10#$2 ));  day=$(( 10#$3 ));  # en bash les variables ne sont pas typees : ex 08 doit etre entier 8 et pas octal
    if (( year % 4 )) ; then BISSEXTILE=0 ; else  BISSEXTILE=1 ; fi
    if (( $((year%100)) == 0 )) ; then BISSEXTILE=0 ; fi
    if (( $((year%400)) == 0 )) ; then BISSEXTILE=1 ; fi
    declare -a calarray=(31 28 31 30 31 30 31 31 30 31 30 31)
    if (( $BISSEXTILE )) ; then calarray[1]=29 ; fi
    JUL=0
    for i in $(seq 0 $((month-2))) ; do a=${calarray[i]} ; JUL=$((JUL+a)) ; done
    JUL=$((JUL+day))
    echo $JUL
         }

JDAY=$(printf "%03d" $(tag2jul $YYYY $MM $DD))

# check if 1day seedfile already exists... and create directories for 1DAY_MSEED_outputs
declare -a STA
declare -a CHAN
declare -a CHNO
CANAL=0
#  l'utilisation du pipe implique la création d'un processus fils. 
#  les variables valorisées au sein de ce processus n'y ont qu'une portée locale.
#  Pour contourner cela, il faut utiliser la substitution de processus
#cat $CURRENT_CHANNELTBL | while read line ; do 
#  CHNO[${CANAL}]=$(echo $line | awk '{print $1}')
#  echo ${CHNO[*]}
#  STA[$CANAL]=$(echo $line | awk '{print $4}' | sed -e "s/N.//")
#  case $(echo $line | awk '{print $5}') in
#    ("E")  CHAN[$CANAL]="EHE" ;;
#    ("N")  CHAN[$CANAL]="EHN" ;;
#    ("U")  CHAN[$CANAL]="EHZ" ;;
#    ("X")  CHAN[$CANAL]="EH3" ;;
#    ("Y")  CHAN[$CANAL]="EH2" ;;
#    ("LE") CHAN[$CANAL]="BAE" ;;
#    ("LN") CHAN[$CANAL]="BAN" ;;
#  esac
# not necessary if we are computing in a oar.$jobid temporary directory (on froggy)
#  if [ -f $ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE/$NET.${STA[$CANAL]}.$LOC.${CHAN[$CANAL]}.$TYPE.$YYYY.$JDAY.gz ] ; then
#    echo "$ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE/$NET.${STA[$CANAL]}.$LOC.${CHAN[$CANAL]}.$TYPE.$YYYY.$JDAY.gz already exists. Abort ..." 
#    exit
#  fi
#  mkdir -p $ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE
#  ((CANAL++))
#done
while read line ; do
  CHNO[${CANAL}]=$(echo $line | awk '{print $1}')
  STA[$CANAL]=$(echo $line | awk '{print $4}' | sed -e "s/N.//")
  case $(echo $line | awk '{print $5}') in
    ("E")  CHAN[$CANAL]="EHE" ;;
    ("N")  CHAN[$CANAL]="EHN" ;;
    ("U")  CHAN[$CANAL]="EHZ" ;;
    ("X")  CHAN[$CANAL]="EH3" ;;
    ("Y")  CHAN[$CANAL]="EH2" ;;
    ("LE") CHAN[$CANAL]="BAE" ;;
    ("LN") CHAN[$CANAL]="BAN" ;;
    ("LX") CHAN[$CANAL]="BA3" ;;
    ("LY") CHAN[$CANAL]="BA2" ;;
  esac
# not necessary if we are computing in a oar.$jobid temporary directory (on froggy)
  if [ -f $ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE/$NET.${STA[$CANAL]}.$LOC.${CHAN[$CANAL]}.$TYPE.$YYYY.$JDAY.gz ] ; then
    echo "$ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE/$NET.${STA[$CANAL]}.$LOC.${CHAN[$CANAL]}.$TYPE.$YYYY.$JDAY.gz already exists. Abort ..." 
    exit
  fi
  mkdir -p $ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE
  ((CANAL++))
done < <(cat $CURRENT_CHANNELTBL)

declare -a HO
for HOUR in $(seq 0 23); do
  HO[${HOUR}]=$(printf "%02d" ${HOUR})
done
declare -a MI
for MIN in $(seq 0 59); do  # change here to convert between 0 and 59
  MI[${MIN}]=$(printf "%02d" ${MIN})
done

win2gzmseed_bychannel(){
  # usage: win2gzmseed_bychannel
  PRE_WINFILE=${WINDIR}/${TAG}
  POST_WINFILE=0101${DATATYPE_A}${DATATYPE_B}.cnt
  for HOUR in $(seq 0 23); do
#    while [ ! -e $LOCK/$CANAL ] ; do
#      sleep $SLEEP_TRAVAILLEUR
#    done
    for MIN in $(seq 0 59); do  # change here to convert between 0 and 59
      WINFILE="${PRE_WINFILE}${HO[${HOUR}]}${MI[${MIN}]}${POST_WINFILE}"
      $WIN2SAC $WINFILE ${CHNO[$CANAL]} - -pwin.prm.$TAG -m$PMAX 2> /dev/null \
	     | $SAC2MSEED - $ENC -n $NET -t ${STA[$CANAL]} -l $LOC -c ${CHAN[$CANAL]} -o - 2> /dev/null
#      $WIN2SAC $WINFILE ${CHNO[$CANAL]} - -pwin.prm.$TAG -m$PMAX 2>> ./${STA[$CANAL]}_${CHAN[$CANAL]}_$CANAL.logerr \
#	     | $SAC2MSEED - $ENC -n $NET -t ${STA[$CANAL]} -l $LOC -c ${CHAN[$CANAL]} -o - 2>> ./${STA[$CANAL]}_${CHAN[$CANAL]}_$CANAL.logerr
#      $WIN2SAC $WINFILE ${CHNO[$CANAL]} - -pwin.prm.$TAG -m$PMAX 2>> ./${STA[$CANAL]}_${CHAN[$CANAL]}_$CANAL.logerr \
#             > ${STA[$CANAL]}_${CHAN[$CANAL]}_${CANAL}_${HOUR}${MIN}.sac
#             cat ${STA[$CANAL]}_${CHAN[$CANAL]}_${CANAL}_${HOUR}${MIN}.sac \
#	     | $SAC2MSEED - $ENC -n $NET -t ${STA[$CANAL]} -l $LOC -c ${CHAN[$CANAL]} -o - 2>> ./${STA[$CANAL]}_${CHAN[$CANAL]}_$CANAL.logerr > ${STA[$CANAL]}_${CHAN[$CANAL]}_${CANAL}_${HOUR}${MIN}.mseed
    done
#    rm $LOCK/$CANAL
  done | gzip --fast -c > $ODIR/$YYYY/$NET/${STA[$CANAL]}/${CHAN[$CANAL]}.$TYPE/$NET.${STA[$CANAL]}.$LOC.${CHAN[$CANAL]}.$TYPE.$YYYY.$JDAY.gz
#  done
                          }
echo "Processing: $TAG 00 -1     Date and Time: $(date +%F" "%T"."%N)"

# loop over the channels
#mkdir -p $LOCK
for CANAL in $(seq 0 $((NBCHANNEL-1))) ; do
  START=$(date +%s.%N)
  echo -n "$((CANAL+1)) / $NBCHANNEL : ${STA[$CANAL]} ${CHAN[$CANAL]} $(date) $START "
  rb0=$(grep "^read_bytes:" /proc/$$/io | awk '{print $2}')
  wb0=$(grep "^write_bytes:" /proc/$$/io | awk '{print $2}')
  win2gzmseed_bychannel  # &
  rb1=$(grep "^read_bytes:" /proc/$$/io | awk '{print $2}')
  wb1=$(grep "^write_bytes:" /proc/$$/io | awk '{print $2}')
  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  rb=$(echo "($rb1 - $rb0)/$DIFF" | bc)
  wb=$(echo "($wb1 - $wb0)/$DIFF" | bc)
  echo "ElapsedTime: $DIFF sec $rb rB/s $wb wB/s "
done

## ensure that max 2 hours of input files will be cached to the memory
#for HOUR in $(seq 0 23); do
#  while [ -e $LOCK/0 ] ; do
#    sleep $SLEEP_ORDONNANCEUR
#  done
#  touch $LOCK/0
#  echo "Processing: $TAG ${HO[${HOUR}]} 00     Date and Time: $(date +%F" "%T"."%N)"
#  for CANAL in $(seq 1 $((NBCHANNEL-1))) ; do
#    while [ -e $LOCK/$CANAL ] ; do
#      sleep $SLEEP_ORDONNANCEUR
#    done
#    touch $LOCK/$CANAL
#  done
#  if (( $HOUR > 1 )) ; then rm ${WINDIR}/${TAG}${HO[$((HOUR-2))]}??0101${DATATYPE_A}${DATATYPE_B}.cnt ; fi
#done
#
## synchronisation
#for CANAL in $(seq 0 $((NBCHANNEL-1))) ; do
#  while [ -e $LOCK/$CANAL ] ; do
#    sleep $SLEEP_ORDONNANCEUR
#  done
#done

#rmdir $LOCK

echo "Processing: $TAG 24 00     Date and Time: $(date +%F" "%T"."%N)"
