#!/bin/bash
#OAR --project f-image
#OAR -l /nodes=1/core=1,walltime=48:00:00
#OAR -n conversion
#OAR -p network_address='luke4'
##OAR -t besteffort

#set -x

# exemple
# oarsub -S "./submit_cntmseed.sh MSEED_20110106_VM_SOUTH.tar 20110106 VM 02"
# oarsub -S "./submit_cntmseed.sh MSEED_20110106_VM.tar 20110106 VM 01"

source /applis/ciment/v2/env.bash
#module load irods/3.1_gcc-4.6.2

# args
if [ $# -ne 4 ]; then
    echo "ERROR: Bad number of parameters"
    echo "USAGE: $0 <output_file> <tag> <datatype> <southtype>"
    exit 1
fi

#RUN_DIR=$HOME/conversion_$CIGRI_CAMPAIGN_ID/

#cd $RUN_DIR

cat $OAR_NODE_FILE

#if [ -e /etc/hostname ] ; then MACHINE=$(cat /etc/hostname) ; fi
#MACHINE=${MACHINE:=0}
#if [ ${MACHINE:0:6} == gofree ] ; then 
#  TMPDIR=$SCRATCH_DIR/$USER/oar.$OAR_JOB_ID
#else # froggy
#  TMPDIR=/tmp/$USER/oar.$OAR_JOB_ID
#fi

#case "$CLUSTER_NAME" in
#  gofree)
#    TMPDIR="$SCRATCH_DIR/$USER/oar.$OAR_JOB_ID"
#    ;;
#  froggy)
#    TMPDIR="/tmp/$USER/oar.$OAR_JOB_ID"
#    if [[ $HOSTNAME == 'froggyfat1' ]] ; then ulimit -u 257559 ; fi
#    ;;
#  luke)
#    TMPDIR="/var/tmp/$USER/oar.$OAR_JOB_ID"
#    #if [[ $HOSTNAME == 'luke1' || $HOSTNAME == 'luke2' ]] ; then echo "$HOSTNAME: not enough space available in local storage, exit 66" ; exit 66 ; fi
#    if [[ $HOSTNAME == 'luke1' || $HOSTNAME == 'luke2' ]] ; then echo "$HOSTNAME: not enough space available in local storage, compute in SHARED_SCRATCH_DIR" ; TMPDIR="$SHARED_SCRATCH_DIR/$USER/oar.$OAR_JOB_ID" ; fi
#    ;;
#esac

CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

TMPDIR=$SHARED_SCRATCH_DIR/$USER/RUN_$CIGRI_CAMPAIGN_ID/oar.$OAR_JOB_ID

mkdir -p $TMPDIR
cp win_skel.prm $TMPDIR/.
cp conversion.bash $TMPDIR/.

TAG=$2
DATATYPE=$3
NS="$4" # 01 ou 02 (02 si SOUTH)

OUTPUTFILE=$1  # MSEED_${TAG}_${DATATYPE}.tar


YYYY=${TAG:0:4}
MM=${TAG:4:2}
DD=${TAG:6:2}
DATATYPE_A=${DATATYPE:0:1}
DATATYPE_B=${DATATYPE:1:1}

cd $TMPDIR

# Rapatrie cnt file from irods
echo "BeginRapatriecnt: $TAG 00 -4     Date and Time: $(date +%F" "%T"."%N)"
#ibun -f -cDtar -R smh-cecic-s3-cigri2 CNT_${TAG}_${DATATYPE}.tar /cigri/home/isterre/$YYYY/$MM/$DD/01/01/${DATATYPE_A}/${DATATYPE_B}
#secure_iget -VKf CNT_${TAG}_${DATATYPE}.tar
#irm CNT_${TAG}_${DATATYPE}.tar
#tar -xf CNT_${TAG}_${DATATYPE}.tar
#RC=0
#secure_iget -rVKf /cigri/home/isterre/$YYYY/$MM/$DD/01/02/${DATATYPE_A}/${DATATYPE_B} || RC=$? && true
##secure_iget -rVKf /cigri/home/isterre/$YYYY/$MM/$DD/01/01/${DATATYPE_A}/${DATATYPE_B} || RC=$? && true
#if [ "$RC" != "0" ] ; then exit $RC ; fi

ln -sf $SHARED_SCRATCH_DIR/$USER/0D586940434DBDBB/$YYYY/$MM/$DD/01/$NS/${DATATYPE_A}/${DATATYPE_B} .
#ln -sf /scratch_md1200/$USER/0D586940434DBDBB/$YYYY/$MM/$DD/01/$NS/${DATATYPE_A}/${DATATYPE_B} .

echo "EndRapatriecnt: $TAG 00 -3     Date and Time: $(date +%F" "%T"."%N)"

./conversion.bash -t $TAG -d $DATATYPE

#ls -l $YYYY/ZZ/*/*/*

if [ "$DATATYPE" == "TL" ] ; then

echo "BeginExportmseed: $TAG 24 01     Date and Time: $(date +%F" "%T"."%N)"
echo "tar -cf ..."
tar --remove-files -cf ${OUTPUTFILE} -C $YYYY .
echo "EndExportmseed:   $TAG 24 05     Date and Time: $(date +%F" "%T"."%N)"

fi

#echo "secure_iput:      $TAG 24 02     Date and Time: $(date +%F" "%T"."%N)"
#RC=0
#secure_iput -R smh-simsu-l4-r1 -KVfDtar ${OUTPUTFILE} conversion_results/$CIGRI_CAMPAIGN_ID || RC=$? && true
#if [ "$RC" != "0" ] ; then exit $RC ; fi
#imeta add -d conversion_results/${CIGRI_CAMPAIGN_ID}/${OUTPUTFILE} OAR_JOB_ID $OAR_JOB_ID
#imeta add -d conversion_results/${CIGRI_CAMPAIGN_ID}/${OUTPUTFILE} CIGRI_CAMPAIGN_ID $CIGRI_CAMPAIGN_ID
#imeta add -d conversion_results/${CIGRI_CAMPAIGN_ID}/${OUTPUTFILE} NODE_NAME `hostname`
#echo "ibun:             $TAG 24 03     Date and Time: $(date +%F" "%T"."%N)"
#ibun -xb conversion_results/$CIGRI_CAMPAIGN_ID/${OUTPUTFILE} $YYYY
#echo "irm:              $TAG 24 04     Date and Time: $(date +%F" "%T"."%N)"
#irm conversion_results/$CIGRI_CAMPAIGN_ID/${OUTPUTFILE}
mkdir -p $SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID
#mv ${OUTPUTFILE} $SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.


#rm -r $TMPDIR
mv $TMPDIR $SHARED_SCRATCH_DIR/$USER/$CIGRI_CAMPAIGN_ID/.
sync

echo "EndJob:   $TAG 24 06     Date and Time: $(date +%F" "%T"."%N)"

