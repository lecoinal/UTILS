#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

###############################################################
# This is the python code make_dataset.py:
# - Read 2D dataset [Nstations,Ntime] from input file on SUMMER
# - Write Nstations 1D dataset[Ntime]
# - Creates ASCII file with metrics: staname,x,y,z
###############################################################

import h5py
import sys
import numpy as np

datatype=np.float32
nb_samples = 900000

idir='.'
ifile=sys.argv[1] 
odir='./out'
ofile=ifile 

h5f = h5py.File(idir+'/'+ifile,'r')
h5fo = h5py.File(odir+'/'+ofile,'w')
f = open(odir+'/zreseau_'+ofile.replace('.h5','.txt'), 'wb')

locations = h5f['locations']
print np.shape(locations)
nb_sta = np.shape(locations)[0]
print(nb_sta)
for ista in range(nb_sta):
	name = 'A'+str(int(locations[ista,0]))+'_'+str(int(locations[ista,1]))
	x = '{0:f}'.format(locations[ista,2])
	y = '{0:f}'.format(locations[ista,3])
	z = '{0:f}'.format(locations[ista,4])
	dataset = h5f['data'][ista,:]
	h5fo.create_dataset(name,shape=(nb_samples,), dtype=datatype,data=dataset[:],compression='gzip', compression_opts=6, fletcher32='True')
	print(ista,' / ',nb_sta,name,x,y,z)
	f.write(name+' '+x+' '+y+' '+z+'\n')
h5f.close()
h5fo.close()
f.close()
