#!/bin/bash
#OAR --project iste-equ-ondes
#OAR -l /nodes=1/cpu=1/core=1,walltime=1:00:00
#OAR -n make_dataset

###############################################################
# This is the submission script to launch make_dataset.py
# on ISterre clusters 
# - Read 2D dataset [Nstations,Ntime] from input file on SUMMER
# - Write Nstations 1D dataset[Ntime]
# Example: 
# $ f="/data/projects/sisprobe/GLENHAVEN/Gosia/DATA_H5/old/2015_06_26-14_00_00.h5"
# $ oarsub -S "./submit_make_dataset.sh $f"
###############################################################

set -e

source /soft/env.bash
module load python/python2.7

f=$1 # '/data/projects/sisprobe/GLENHAVEN/Gosia/DATA_H5/old/2015_06_26-14_00_00.h5'
cat $OAR_NODE_FILE
echo "$SHARED_SCRATCH_DIR"
TMPDIR="$SHARED_SCRATCH_DIR/$USER/GOSIA/oar.$OAR_JOB_ID"
echo $TMPDIR
mkdir -p $TMPDIR

cp make_dataset.py $TMPDIR/.
cd $TMPDIR/.

ln -sf $f .
mkdir out
python2.7 -u make_dataset.py $(basename $f)
