#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

###############################################################
# This is the python code repack_dataset.py:
# - Read 3D velocity model H5file
# - change gridlocs from float64 to int32, add gzip compression
# - change each gridcell ttgr[Nr] from float32 to int32, add gzip compression
# - remove the 5 dummy stations
# - change ttgr into ttgr/distxgr (which is inverse_vapp)
###############################################################

# source /applis/ciment/v2/env.bash
# module load python/2.7.12_gcc-4.6.2
# python2.7 repack_dataset.py GridTrav3.h5 
# python2.7 repack_dataset.py travelTimeTable3Dv2.h5 32 0 

import h5py
import sys
import numpy as np

# options:
# - dtype for ttgr/inv_vapp output writing
ln_datatype = int(sys.argv[2]) # 64 # 32 # np.float64 # 32
# - activate option if we want inv_vapp (= ttgr/distxgr) instead of ttgr
lk_inv_vapp = int(sys.argv[3]) # 1
# - gridcells
icb = 1 # 2000001
ice = 3154001 # 4000000 # 2000002

input_name_dir="ttimes" # "GridCell"

#########################

if ln_datatype == 64:
	datatype = np.float64
else:
	datatype = np.float32

print(datatype)

#idir='.'
ifile=sys.argv[1] # /summer/sisprobe/ftp/Sisprobe-Glenhaven/travelTimeTable3Dv2.h5 
odir='./out'
ofile="travelTimeTable3Dv2_"+str(lk_inv_vapp)+"_"+str(ln_datatype)+".h5" # ifile 

#h5f = h5py.File(idir+'/'+ifile,'r')
h5f = h5py.File(ifile,'r')
h5fo = h5py.File(odir+'/'+ofile,'w')

gridlocs = h5f['gridlocs']
gl=gridlocs[()]
print np.shape(gridlocs)
print np.min(gl[0,:]),np.min(gl[1,:]),np.min(gl[2,:])
print np.max(gl[0,:]),np.max(gl[1,:]),np.max(gl[2,:])
h5fo.create_dataset('/gridlocs',shape=np.shape(gridlocs), dtype=np.int32,data=gridlocs[:,:],compression='gzip', compression_opts=6, fletcher32='True')

#ordre alphabetique de station en évitant les doublon:
#station en trop a retirer du modele de vitesse
# lecointre@luke:/bettik/lecointre/GOSIA/3DVelModel/Final_4$ grep -n -v -f orig.txt staname.txt 
# 781:A1030_5196
# 1224:A1050_5081
# 5674:A1200_5122
# 6669:A1230_5188
# 8402:A1290_5016

sl = h5f['stationLocs'][()]
print np.shape(sl)   # here we have the 5 dummy stations

indice_remove = np.array([780,1223,5673,6668,8401])

#for icell in range(2000001,3000001):
for icell in range(icb,ice):
	ttcell = h5f[input_name_dir+'/'+str(icell)][()]
	if lk_inv_vapp == 1:
		xgr = gl[:,icell-1]   # Caution 0-based (python convention) versus 1-based (dataset name convention)
		distxgr = np.sqrt( (xgr[0]-sl[0,:])**2 + (xgr[1]-sl[1,:])**2 + (xgr[2]-sl[2,:])**2 )
		ttcell = np.divide(ttcell[:],distxgr[:]) # inv_vapp
        tc = np.delete(ttcell, indice_remove)
	print icell,np.shape(ttcell),np.shape(tc)
	h5fo.create_dataset('GridCell/'+'{:07d}'.format(icell),shape=(10050,), dtype=datatype,data=tc[:],compression='gzip', compression_opts=6, fletcher32='True')

h5f.close()
h5fo.close()
