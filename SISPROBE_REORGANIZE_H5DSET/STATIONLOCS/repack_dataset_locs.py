#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

###############################################################
# This is the python code repack_dataset_locs.py:
# - Read 3D velocity model H5file: stationLocs : x,y,z
# - remove the 5 dummy stations
###############################################################

# source /applis/ciment/v2/env.bash
# module load python/2.7.12_gcc-4.6.2
# h5copy -i travelTimeTable3Dv2.h5 -o StationLocs.h5 -s /stationLocs -d /stationLocs
# python2.7 repack_dataset_locs.py

import h5py
import numpy as np

#########################

ifile="StationLocs.h5" # sys.argv[1] # /summer/sisprobe/ftp/Sisprobe-Glenhaven/travelTimeTable3Dv2.h5 
odir='./out'
ofile=ifile 

h5f = h5py.File(ifile,'r')
h5fo = h5py.File(odir+'/'+ofile,'w')

#ordre alphabetique de station en évitant les doublon:
#station en trop a retirer du modele de vitesse
# lecointre@luke:/bettik/lecointre/GOSIA/3DVelModel/Final_4$ grep -n -v -f orig.txt staname.txt 
# 781:A1030_5196
# 1224:A1050_5081
# 5674:A1200_5122
# 6669:A1230_5188
# 8402:A1290_5016

sl = h5f['stationLocs'][()]
print np.shape(sl)   # here we have the 5 dummy stations

sloo=np.zeros((3,10050),np.float64)

indice_remove = np.array([780,1223,5673,6668,8401])

for ii in range(3):
	sli = sl[ii,:]
	slo = np.delete(sli, indice_remove)
	sloo[ii,:] = slo[:]

h5fo.create_dataset('stationLocs',shape=(3,10050), dtype=np.float64,data=sloo[:,:],compression='gzip', compression_opts=6, fletcher32='True')

h5f.close()
h5fo.close()
